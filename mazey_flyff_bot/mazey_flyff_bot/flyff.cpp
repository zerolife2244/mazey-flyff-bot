#include "consts.h"
#include "flyff.h"
#include "prints.h"
#include "search.h"

#include <ctime>

const char flyffProcessName[] = "Neuz.exe";
const char flyffName[] = "Mazey Flyff";

const char meAddrSignature[] = "\x81\xEC\xCC\x00\x00\x00\x56";
const char selectAddrSignature[] = "\xDB\x04\x01";
const char targetBaseAddrSignature[] = "\xFF\x00\x00\x00\x00\x6A\x01\x6A\x1B";
const char rangeAddrSignature[] = "\x6A\x00\x6A\x5A";
const char noCollisionAddrSignature[] = "\xC3\xCC\x55\x8B\xEC\x83\xEC\x64";

const unsigned int OFFSET_NAME = 0x1780;
const unsigned int OFFSET_SELECT = 0x20;
const unsigned int OFFSET_X = 0x164;
const unsigned int OFFSET_LVL = 0x6E4;
const unsigned int OFFSET_HP = 0x708;
const unsigned int OFFSET_TYPE = 0x4;

const unsigned long OFFSET_RANGE_ALL = 0xC97;

unsigned long __stdcall bot_thread(void *t) {
  bool killed;
  time_t time_selected;
  unsigned long bad_target;
  unsigned int killed_count;

  flyff *f = (flyff *)t;
  killed = false;
  killed_count = 0;

  // sleep to hopefully sync printfs
  Sleep(10);

  f->select(0);
  for (;; Sleep(100)) {
    // deal with killing
    if (f->get_select() == 0) {
      if (killed == false) {
        if (f->tele_back_home && killed_count >= f->tele_back_home_targets) {
          f->teleport_to_home();
          killed_count = 0;
          Sleep(1000);
        }

        killed_count++;
      } // if (killed == false)

      // select target if any
      unsigned long target = f->get_closest_target_in_view();

      if (target != 0 && target != bad_target) {
        printf("select target: %08X '%s'\n", target, f->get_name(target));

        f->select(target);

        // get time we select target
        time_selected = time(0);

        if (f->tele_to_target) {
          f->teleport_target_to(f->get_me(), target, true);
        }

        killed = false;
      } else {
        // rotate cam
        PostMessage((HWND)f->hwnd, WM_KEYDOWN, VK_RIGHT,
                    MapVirtualKey(VK_RIGHT, MAPVK_VK_TO_VSC));
        Sleep(50);
        PostMessage((HWND)f->hwnd, WM_KEYUP, VK_RIGHT,
                    MapVirtualKey(VK_RIGHT, MAPVK_VK_TO_VSC));
        Sleep(50);
      }
    } else {
      if (f->tele_target_to_player) {
        f->teleport_target_to(f->get_select(), f->get_me(), false);
      }

      // press killing key
      PostMessage((HWND)f->hwnd, WM_KEYDOWN, f->bot_key_code,
                  MapVirtualKey(f->bot_key_code, MAPVK_VK_TO_VSC));
      Sleep(50);
      PostMessage((HWND)f->hwnd, WM_KEYUP, f->bot_key_code,
                  MapVirtualKey(f->bot_key_code, MAPVK_VK_TO_VSC));
      Sleep(50);

      // check time we have time to kill target
      if (f->reselect_after > 0) {
        time_t now = time(0);

        if (time_selected + f->reselect_after < now) {
          // if we passed time we had to kill target then select 0
          bad_target = f->get_select();
          f->select(0);

          printf("couldn't killd in %d seconds, reselcting target\n",
                 f->reselect_after);
        } // if (time_selected + reselect_after < now)
      }   // if (reselect_after > 0)
    }     // if (f->get_select() == 0)
  }       // for (;; Sleep(100))
}

struct handle_data {
  unsigned long process_id;
  HWND window_handle;
};

BOOL is_main_window(HWND handle) {
  return GetWindow(handle, GW_OWNER) == (HWND)0 && IsWindowVisible(handle);
}

BOOL CALLBACK enum_windows_callback(HWND handle, LPARAM lParam) {
  handle_data &data = *(handle_data *)lParam;
  unsigned long process_id = 0;
  GetWindowThreadProcessId(handle, &process_id);
  if (data.process_id != process_id || !is_main_window(handle))
    return TRUE;
  data.window_handle = handle;
  return FALSE;
}

HWND find_main_window(unsigned long process_id) {
  handle_data data;
  data.process_id = process_id;
  data.window_handle = 0;
  EnumWindows(enum_windows_callback, (LPARAM)&data);
  return data.window_handle;
}

std::vector<playerInfo *> flyff::get_players() {
  void *handle;
  unsigned long me_addr;
  std::vector<playerInfo *> players;

  void *snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  PROCESSENTRY32 pe32;
  pe32.dwSize = sizeof(PROCESSENTRY32);

  while (Process32Next(snapshot, &pe32)) {
    if (_stricmp(pe32.szExeFile, flyffProcessName) == 0) {
      handle =
          OpenProcess(PROCESS_VM_OPERATION | PROCESS_VM_READ | PROCESS_VM_WRITE,
                      false, pe32.th32ProcessID);

      if (handle) {
        unsigned long addr;
        unsigned long base;
        unsigned long pointed;

        base = get_module(handle, flyffProcessName);
        addr =
            search(handle, base, 0x1FFFFFFF, flyffName, strlen(flyffName), 1);

        if (addr) {
          printf("found %s: %08X\n", flyffName, pe32.th32ProcessID);

          // search for addresses

          printf("searching for `me_addr` ... ");
          addr = search(handle, base, 0x1FFFFFFF, meAddrSignature,
                        sizeof(meAddrSignature) / sizeof(char) - 1, 1);
          if (addr) {
            ReadProcessMemory(handle, (void *)(addr + 0x2C + 2), &me_addr, 4,
                              0);
            printf("%08X Done\n", me_addr);

            // get local player name and add it to array
            char *player_name = (char *)malloc(256);
            ReadProcessMemory(handle, (void *)(me_addr), &pointed, 4, 0);
            ReadProcessMemory(handle, (void *)(pointed + OFFSET_NAME),
                              player_name, 256, 0);

            if (pointed != 0x0 && player_name[0] != 0x00) {
              // make new player info struct
              playerInfo *pi = (playerInfo *)malloc(sizeof(playerInfo));
              pi->name = player_name;
              pi->pid = pe32.th32ProcessID;

              // and add it to vector
              players.push_back(pi);
            } else {
              printError("couldn't get local player name");

              // free create player name cause we didn't get any
              free(player_name);
            }
          } else {
            printf("Failed\n");
          } // if (addr)
        } else {
          printf("didn't find `%s` string from pid: %08X\n", flyffName,
                 pe32.th32ProcessID);
        } // if (addr)
      }   // if (handle)
    }     // if (_stricmp(pe32.szExeFile, flyffProcessName) == 0)
  }       // while (Process32Next(snapshot, &pe32))

  CloseHandle(snapshot);

  return players;
}

flyff::flyff() {}

flyff::flyff(unsigned long pid) {
  unsigned long addr;

  handle = OpenProcess(
      PROCESS_VM_OPERATION | PROCESS_VM_READ | PROCESS_VM_WRITE, false, pid);

  if (handle) {
    hwnd = find_main_window(pid);
    base = get_module(handle, flyffProcessName);

    // searching addresses

    printf("searching for `me_addr` ... ");
    addr = search(handle, base, 0x1FFFFFFF, meAddrSignature,
                  sizeof(meAddrSignature) / sizeof(char) - 1, 1);

    if (addr) {
      ReadProcessMemory(handle, (void *)(addr + 0x2C + 2), &me_addr, 4, 0);
      printf("%08X Done\n", me_addr);
    } else {
      printf("Failed\n");
    } // if (addr)

    printf("searching for `select_addr` ... ");
    addr = search(handle, base, 0x1FFFFFFF, selectAddrSignature,
                  sizeof(selectAddrSignature) / sizeof(char) - 1, 1);

    if (addr) {
      ReadProcessMemory(handle, (void *)(addr - (0x6E - 2)), &select_addr, 4,
                        0);
      printf("%08X Done\n", select_addr);
    } else {
      printf("Failed\n");
    } // if (addr)

    printf("searching for `target_base_addr` and `max_in_view_addr` ... ");
    addr = search(handle, base, 0x1FFFFFFF, targetBaseAddrSignature,
                  sizeof(targetBaseAddrSignature) / sizeof(char) - 1, 1);

    if (addr) {
      ReadProcessMemory(handle, (void *)(addr + (0x47 + 3 - 5)),
                        &target_base_addr, 4, 0);
      ReadProcessMemory(handle, (void *)(addr + (0x38 + 2 - 5)),
                        &max_in_view_addr, 4, 0);

      printf("%08X and %08X Done\n", target_base_addr, max_in_view_addr);
    } else {
      printf("Failed\n");
    } // if (addr)

    printf("searching for `range_addr` ... ");
    addr = search(handle, base, 0x1FFFFFFF, rangeAddrSignature,
                  sizeof(rangeAddrSignature) / sizeof(char) - 1, 1);

    if (addr) {
      range_addr = addr - 0xAF;

      printf("%08X Done\n", range_addr);
    } else {
      printf("Failed\n");
    } // if (addr)

    printf("searching for `no_collision_addr` ... ");
    addr = search(handle, base, 0x1FFFFFFF, noCollisionAddrSignature,
                  sizeof(noCollisionAddrSignature) / sizeof(char) - 1, 1);

    if (addr) {
      no_collision_addr = addr + 0x546 -1;
      printf("%08X Done\n", no_collision_addr);
    } else {
      printf("Failed\n");
    } // if (addr)

    // default some vars
    bot_thread_handle = nullptr;
    target_lvl_begin = 2;
    target_lvl_end = 200;
    bot_key_code = keys[0].code; // F1
    reselect_after = reselect_after_times[0];
    tele_back_home = false;
    tele_back_home_targets = 3;
    tele_to_target = false;

    hasErrors = false;
  } else {
    printError("can't open proccess by pid: %08X", pid);
    hasErrors = true;
  }
}

unsigned long flyff::get_me() {
  unsigned long pointed;
  ReadProcessMemory(handle, (void *)me_addr, &pointed, 4, 0);
  return pointed;
}

void flyff::save_home() {
  ReadProcessMemory(handle, (void *)(get_me() + OFFSET_X), home, 12, 0);
  home[1] += 3.f;
  printf("`home` = [%f, %f, %f]\n", home[0], home[1], home[2]);
}

void flyff::teleport_to_home() {
  printf("teleport to [%f, %f, %f]\n", home[0], home[1], home[2]);
  WriteProcessMemory(handle, (void *)(get_me() + OFFSET_X), home, 12, 0);
}

void flyff::teleport_target_to(unsigned long target, unsigned long to, bool server_update) {
  float pos[3];
  ReadProcessMemory(handle, (void *)(to + OFFSET_X), pos, 12, 0);
  if (server_update) {
    pos[1] += 3.f;
  }

  WriteProcessMemory(handle, (void *)(target + OFFSET_X), pos, 12, 0);
}

float flyff::get_hyp(unsigned long target) {
  float target_xyz[3];
  float me_xyz[3];

  ReadProcessMemory(handle, (void *)(target + OFFSET_X), target_xyz, 12, 0);
  ReadProcessMemory(handle, (void *)(get_me() + OFFSET_X), me_xyz, 12, 0);

  return sqrt(pow((me_xyz[0] - target_xyz[0]), 2) +
              pow((me_xyz[2] - target_xyz[2]), 2));
}

void flyff::select(unsigned long target) {
  unsigned long pointed;

  ReadProcessMemory(handle, (void *)select_addr, &pointed, 4, 0);

  if (pointed) {
    WriteProcessMemory(handle, (void *)(pointed + OFFSET_SELECT), &target, 4,
                       0);
  } else {
    printError("`select_addr` pointed to %08X", pointed);
  }
}

unsigned long flyff::get_select() {
  unsigned long pointed;

  ReadProcessMemory(handle, (void *)select_addr, &pointed, 4, 0);

  if (pointed) {
    ReadProcessMemory(handle, (void *)(pointed + OFFSET_SELECT), &pointed, 4,
                      0);
  } else {
    printError("`select_addr` pointed to %08X", pointed);
  }

  return pointed;
}

unsigned long flyff::get_closest_target_in_view() {
  unsigned long max_in_view;
  unsigned long target;
  unsigned long type;
  unsigned long lvl;
  unsigned long hp;

  unsigned long closest_target;
  float closest_hyp;

  max_in_view = 0;
  closest_target = 0;
  closest_hyp = 99999999.f;

  // get pointed addrs
  ReadProcessMemory(handle, (void *)(max_in_view_addr), &max_in_view, 4, 0);

  // printf("`max_in_view`: %d\n", max_in_view);

  for (unsigned long i = 1; i < max_in_view; i++) {
    target = 0;
    type = 0;
    lvl = 0;
    hp = 0;

    ReadProcessMemory(handle, (void *)(i * 4 + target_base_addr), &target, 4,
                      0);
    ReadProcessMemory(handle, (void *)(target + OFFSET_TYPE), &type, 4, 0);
    ReadProcessMemory(handle, (void *)(target + OFFSET_LVL), &lvl, 4, 0);
    ReadProcessMemory(handle, (void *)(target + OFFSET_HP), &hp, 4, 0);

    // printf("base: %08X\ntarget: %08X\ntype: %d\nlvl: %d\nhp: %d\n\n",
    //       i * 4 + target_base_addr, target, type, lvl, hp);

    if (type == 18 && lvl >= target_lvl_begin && lvl <= target_lvl_end &&
        hp > 1) {
      float hyp = get_hyp(target);

      if (hyp < closest_hyp) {
        closest_target = target;
        closest_hyp = hyp;
      }
    }
  }

  return closest_target;
}

char *flyff::get_name(unsigned long target) {
  char *player_name = (char *)malloc(256);

  ReadProcessMemory(handle, (void *)(target + OFFSET_NAME), player_name, 256,
                    0);

  return player_name;
}

void flyff::run_bot() {
  bot_thread_handle = CreateThread(0, 0, bot_thread, this, 0, 0);
}

void flyff::stop_bot() {
  TerminateThread(bot_thread_handle, 0);
  bot_thread_handle = nullptr;
}

bool flyff::is_bot_running() { return bot_thread_handle != nullptr; }

void flyff::activate_range() {
  WriteProcessMemory(handle, (void *)range_addr,"\xEB\x0A", 2, 0);
  WriteProcessMemory(handle, (void *)(range_addr + 0x12),"\x6F", 1, 0);

  WriteProcessMemory(handle, (void *)(range_addr + OFFSET_RANGE_ALL),
                     "\x90\x90", 2, 0);
}

void flyff::terminate_range() {
  WriteProcessMemory(handle, (void *)range_addr, "\x77\x49", 2, 0);
  WriteProcessMemory(handle, (void *)(range_addr + 0x12), "\x3F", 1, 0);

  WriteProcessMemory(handle, (void *)(range_addr + OFFSET_RANGE_ALL),
                     "\x75\x15", 2, 0);
}

bool flyff::is_range_activated() {
  unsigned char buf;
  ReadProcessMemory(handle, (void *)range_addr, &buf, 1, 0);

  return buf == 0xEB;
}

void flyff::activate_no_collision() {
  WriteProcessMemory(handle, (void *)no_collision_addr, "\x01", 1, 0);
}

void flyff::terminate_no_collision() {
  WriteProcessMemory(handle, (void *)no_collision_addr, "\x00", 1, 0);
}

bool flyff::is_no_collision_activated() {
  unsigned char buf;
  ReadProcessMemory(handle, (void *)no_collision_addr, &buf, 1, 0);

  return buf;
}