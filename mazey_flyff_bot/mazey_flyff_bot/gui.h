#ifndef GUI_H
#define GUI_H

#include <vector>

#include <nana/gui.hpp>
#include <nana/gui/widgets/button.hpp>
#include <nana/gui/widgets/label.hpp>
#include <nana/gui/widgets/combox.hpp>
#include <nana/gui/widgets/listbox.hpp>
#include <nana/gui/widgets/textbox.hpp>
#include <nana/gui/widgets/checkbox.hpp>

#include "flyff.h"
#include "prints.h"

class gui {
  private:
    // window form
    nana::form *fm;
    // window nested form
    nana::nested_form *nfm;

    // main menu list
    nana::listbox *ml;

    // bot tab variables:
    nana::label *bot_lb_title;
    nana::label *bot_lb_min;
    nana::label *bot_lb_max;
    nana::label *bot_lb_attack_key;
    nana::label *bot_lb_reselct_after;

    // miscs tab variables:
    nana::label *miscs_lb_title;


    // flyff class wheres function to cheat
    flyff *f;

  public:
    gui();

    ~gui() {
      printf("delete `gui{}` ... ");

      // remove tabs crashable vars
      destroy_bot();
      destroy_miscs();

      printf("Done\n");
    }

    void destroy_bot() {
      printf("`destroy_bot()` ... ");

      try {
        // remove bot tab crashable vars
        if (bot_lb_title)
          delete bot_lb_title;
        if (bot_lb_min)
          delete bot_lb_min;
        if (bot_lb_max)
          delete bot_lb_max;
        if (bot_lb_attack_key)
          delete bot_lb_attack_key;
        if (bot_lb_reselct_after)
          delete bot_lb_reselct_after;

        printf("Done\n");
      } catch (const std::exception &e) {
        // dunno why
        printf("\n");
        printError("got exception %s", e.what());
      }
    }

    void destroy_miscs() {
      printf("`destroy_miscs()` ... ");

      try {
        // remove miscs tab crashable vars
        if (miscs_lb_title)
          delete miscs_lb_title;

        printf("Done\n");
      } catch (const std::exception &e) {
        printf("\n");
        printError("got exception %s", e.what());
      }
    }

    void select_char(std::vector<playerInfo *> players);
    void main();
    void bot();
    void miscs();
};

#endif