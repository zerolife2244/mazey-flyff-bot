#ifndef FLYFF_H
#define FLYFF_H

#include <vector>

struct playerInfo {
  char *name;
  unsigned long pid;

  ~playerInfo() {
    if (name)
      free(name);
  }
};

class flyff {
private:
  unsigned long pid;
  void *handle;
  unsigned long base;

  unsigned long me_addr;
  unsigned long select_addr;
  unsigned long target_base_addr;
  unsigned long max_in_view_addr;
  unsigned long range_addr;
  unsigned long no_collision_addr;

  float home[3];

  void *bot_thread_handle;

public:
  bool hasErrors;

  // flyff window vars
  void *hwnd;

  // bot vars, laz to do get and set
  unsigned int target_lvl_begin;
  unsigned int target_lvl_end;
  unsigned long bot_key_code;
  unsigned int reselect_after;
  bool tele_back_home;
  unsigned int tele_back_home_targets;
  bool tele_to_target;
  bool tele_target_to_player;

  static std::vector<playerInfo *> get_players();

  flyff();
  flyff(unsigned long pid);

  unsigned long get_me();
  void save_home();
  void teleport_to_home();
  void teleport_target_to(unsigned long target, unsigned long to,
                          bool server_update);

  float get_hyp(unsigned long target);
  void select(unsigned long target);
  unsigned long get_select();
  unsigned long get_closest_target_in_view();
  char *get_name(unsigned long target);

  void run_bot();
  void stop_bot();
  bool is_bot_running();

  void activate_range();
  void terminate_range();
  bool is_range_activated();

  void activate_no_collision();
  void terminate_no_collision();
  bool is_no_collision_activated();
};

#endif