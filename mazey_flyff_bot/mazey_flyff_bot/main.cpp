#include <vector>

#include "gui.h"
#include "flyff.h"

int main() {
  std::vector<playerInfo *> players = flyff::get_players();


  gui *g = new gui();
  g->select_char(players);
  players.clear();
  players.shrink_to_fit();

  nana::exec();
  delete g;
}