#include "gui.h"
#include "prints.h"
#include "consts.h"

#include <stdlib.h> 

gui::gui() {
  fm = new nana::form(nana::rectangle(nana::size(600, 250)),
                      nana::appearance(1, 1, 1, 1, 1, 1, 1));
  fm->caption("simple bot for Mazey Flyff by lava");

  fm->show();
}

void gui::select_char(std::vector<playerInfo *> players) {
  delete nfm;

  nfm = new nana::nested_form(*fm, nana::appearance(0, 0, 0, 0, 0, 0, 0));

  nana::combox *cmbx_chars = new nana::combox(*nfm, 1);
  nana::button *btn_select = new nana::button(*nfm, "select", 1);

  // add player names to combox
  for (unsigned int i = 0; i < players.size(); i++) {
    cmbx_chars->push_back(players[i]->name);
  }

  btn_select->events().click([this, players, cmbx_chars, btn_select] {
    // search player by selected name
    for (unsigned int i = 0; i < players.size(); i++) {
      if (strcmp(cmbx_chars->caption().c_str(), players[i]->name) == 0) {
        printf("selected player: %s\n", players[i]->name);

        f = new flyff(players[i]->pid);

        // check if we can even open selected character
        if (!f->hasErrors) {
          // create new view
          main();
        } else {
          printError("can't open flyff anymore by player name: %s",
                      players[i]->name);
        }

        break;
      }
    }
  });


  fm->div("<nfm>");
  (*fm)["nfm"] << *nfm;

  nfm->div("vert <><weight=21 margin=2 <><weight=200 cmbx_chars><>><weight=21 "
           "margin=2 <><weight=50 btn_select><>><>");
  (*nfm)["cmbx_chars"] << *cmbx_chars;
  (*nfm)["btn_select"] << *btn_select;

  nfm->collocate();
  fm->collocate();
}

void gui::main() {
  delete nfm;

  nfm = new nana::nested_form(*fm, nana::appearance(0, 0, 0, 0, 0, 0, 0));
  ml = new nana::listbox(*fm, true);
  ml->append_header("Tools", 90);
  ml->show_header(false);
  ml->at(0).append("Bot");
  ml->at(0).append("Miscs");

  ml->events().click([this] {
    // if selected first: Bot
    if (ml->at(0).at(0).selected() == true) {
      printf("selected `Bot` tab\n");
      destroy_bot();
      destroy_miscs();
      bot();
    } else {
      printf("selected `Miscs` tab\n");
      destroy_bot();
      destroy_miscs();
      miscs();
    }
  });

  fm->div("vert <<weight=100 main_list><nfm>>");
  (*fm)["main_list"] << *ml;
  (*fm)["nfm"] << *nfm;

  nfm->collocate();
  fm->collocate();
}

void gui::bot() {
  char buf[2048];
  delete nfm;

  nfm = new nana::nested_form(*fm, nana::appearance(0, 0, 0, 0, 0, 0, 0));

  bot_lb_title = new nana::label(*nfm, "Target level range to select", true);
  bot_lb_title->text_align(nana::align::center, nana::align_v::center);

  bot_lb_min = new nana::label(*nfm, "Min", true);
  bot_lb_min->text_align(nana::align::center, nana::align_v::center);

  bot_lb_max = new nana::label(*nfm, "Max", true);
  bot_lb_max->text_align(nana::align::center, nana::align_v::center);

  nana::textbox *txt_min = new nana::textbox(*nfm, true);
  sprintf(buf, "%d", f->target_lvl_begin);
  txt_min->caption(buf);

  nana::textbox *txt_max = new nana::textbox(*nfm, true);
  sprintf(buf, "%d", f->target_lvl_end);
  txt_max->caption(buf);

  bot_lb_attack_key = new nana::label(*nfm, "Attack key", true);
  bot_lb_attack_key->text_align(nana::align::center, nana::align_v::center);

  nana::combox *cmbx_keys = new nana::combox(*nfm, 1);
  for (unsigned int i = 0; i < keys.size(); i++) {
    if (keys[i].code == f->bot_key_code) {
      cmbx_keys->caption(keys[i].name);
    }

    cmbx_keys->push_back(keys[i].name);
  }

  bot_lb_reselct_after = new nana::label(*nfm, "Re-select target after (seconds)", true);
  bot_lb_reselct_after->text_align(nana::align::center, nana::align_v::center);

  nana::combox *cmbx_reselect_after = new nana::combox(*nfm, 1);
  for (unsigned int i = 0; i < reselect_after_times.size(); i++) {
    sprintf(buf, "%d", reselect_after_times[i]);
    cmbx_reselect_after->push_back(buf);
  }

  sprintf(buf, "%d", f->reselect_after);
  cmbx_reselect_after->caption(buf);


  nana::checkbox *chbx_tele_home = new nana::checkbox(
      *nfm, "Teleport back to home when reached count and restart", true);
  chbx_tele_home->check(f->tele_back_home);

  nana::textbox *txt_tele_home = nullptr;
  if (f->tele_back_home) {
    txt_tele_home = new nana::textbox(*nfm, true);
    sprintf(buf, "%d", f->tele_back_home_targets);
    txt_tele_home->caption(buf);

    txt_tele_home->events().text_changed([this, txt_tele_home] { 
      signed int tele_home_targets;
      std::string str;

      txt_tele_home->getline(0, str);

      if (strcmp(str.c_str(), "") != 0) {
        tele_home_targets = atoi(str.c_str());

        if (tele_home_targets > 0) {
          f->tele_back_home_targets = tele_home_targets;
        } else {
          printError("teleport back to home after killing targets: %s has to "
                     "be over 0", str.c_str());
        }
      }
    });
  }

  nana::checkbox *chbx_tele_to_target = new nana::checkbox(
      *nfm, "Teleport to target when bot selects target", true);
  chbx_tele_to_target->check(f->tele_to_target);

  nana::checkbox *chbx_tele_target = new nana::checkbox(
      *nfm, "Teleport target to local player when bot selects target", true);
  chbx_tele_target->check(f->tele_target_to_player);

  nana::button *btn_enable = new nana::button(*nfm, "Enable", true);
  btn_enable->caption(f->is_bot_running() ? "Disable" : "Enable");

  txt_min->events().text_changed([this, txt_min] {
    unsigned long min;
    std::string str_min;

    txt_min->getline(0, str_min);
    if (strcmp(str_min.c_str(), "") != 0) {
      min = atoi(str_min.c_str());

      if (min > 1) {
        if (min <= f->target_lvl_end) {
          printf("set `f->target_lvl_begin`: %d\n", min);
          f->target_lvl_begin = min;
        } else {
          printError("minimum lvl has to be less or equal to maximum\n\tnew min: %dn\n\tmax: %d",
                     min, f->target_lvl_end);
        }
      } else {
        printError("minimum lvl is 2 to filter out objects and npc\n\tnew min: %d\n\tmax: %d",
                   min, f->target_lvl_end);
      }
    }
  });

  txt_max->events().text_changed([this, txt_max] {
    unsigned long max;
    std::string str_max;

    txt_max->getline(0, str_max);

    if (strcmp(str_max.c_str(), "") != 0) {
      max = atoi(str_max.c_str());

      if (max >= f->target_lvl_begin) {
        printf("set `f->target_lvl_end`: %d\n", max);
        f->target_lvl_end = max;
      } else {
        printError("maximum lvl has to be over or equal to minumum\n\tnew max: %d\n\tmin: %d",
                   max, f->target_lvl_begin);
      }
    }
  });

  cmbx_keys->events().selected([this, cmbx_keys] {
    for (unsigned int i = 0; i < keys.size(); i++) {
      if (strcmp(keys[i].name, cmbx_keys->caption().c_str()) == 0) {
        printf("change `f->bot_key_code`: %s %02X\n", keys[i].name,
               keys[i].code);

        f->bot_key_code = keys[i].code;
        break;
      }
    }
  });

  cmbx_reselect_after->events().selected([this, cmbx_reselect_after] {
    char buf[33];

    for (unsigned int i = 0; i < reselect_after_times.size(); i++) {
      sprintf(buf, "%d", reselect_after_times[i]);

      if (strcmp(buf, cmbx_reselect_after->caption().c_str()) == 0) {
        printf("change `f->reselect_after`: %d\n", reselect_after_times[i]);

        f->reselect_after = reselect_after_times[i];
        break;
      }
    }
  });

  chbx_tele_home->events().checked([this, chbx_tele_home, txt_tele_home] {
    printf("change `f->tele_back_home`: %s\n",
           chbx_tele_home->checked() ? "True" : "False");

    f->tele_back_home = chbx_tele_home->checked();

    if (f->tele_back_home == true) {
      f->save_home();
    }

    // remove label controls
    destroy_bot();

    // reload all
    bot();
  });

  chbx_tele_to_target->events().checked([this, chbx_tele_to_target] {
    printf("change `f->tele_to_target`: %s\n",
           chbx_tele_to_target->checked() ? "True" : "False");

    f->tele_target_to_player = false;
    f->tele_to_target = chbx_tele_to_target->checked();

    // remove label controls
    destroy_bot();

    // reload all
    bot();
  });

  chbx_tele_target->events().checked([this, chbx_tele_target] {
    printf("change `f->chbx_tele_target`: %s\n",
           chbx_tele_target->checked() ? "True" : "False");

    f->tele_to_target = false;
    f->tele_target_to_player = chbx_tele_target->checked();

    // remove label controls
    destroy_bot();

    // reload all
    bot();
  });

  btn_enable->events().click([this] {
    if (f->is_bot_running()) {
      printf("stop bot\n");
      f->stop_bot();
    } else {
      printf("run bot\n");
      f->run_bot();
    }

    // remove label controls
    destroy_bot();

    // reload all
    bot();
  });

  (*fm)["nfm"] << *nfm;

  sprintf(
      buf,
      "vert <><weight=24 lb_title><weight=24 <><weight=30 "
      "lb_min><weight=100 margin=2 txt_min><weight=30 lb_max><weight=100 "
      "margin=2 txt_max><>>"
      "<weight=21 <><weight=75 lb_attack_key><weight=50 margin=2 cmbx_keys><>>"
      "<weight=21 <><weight=190 lb_reselct_after><weight=50 margin=2 "
      "cmbx_reselect_after><>>"
      // checkboxes
      "<weight=24 margin=[0,0,0,4] <chbx_tele_home>%s>"
      "<weight=24 margin=[0,0,0,4] chbx_tele_to_target>"
      "<weight=24 margin=[0,0,0,4] chbx_tele_target>"
      "<><weight=24 "
      "margin=2 btn_enable>", 
    f->tele_back_home ? "<weight=30 margin=2 txt_tele_home>" : "");

  nfm->div(buf);

  (*nfm)["lb_title"] << *bot_lb_title;
  (*nfm)["lb_min"] << *bot_lb_min;
  (*nfm)["txt_min"] << *txt_min;
  (*nfm)["lb_max"] << *bot_lb_max;
  (*nfm)["txt_max"] << *txt_max;

  (*nfm)["lb_attack_key"] << *bot_lb_attack_key;
  (*nfm)["cmbx_keys"] << *cmbx_keys;
  (*nfm)["lb_reselct_after"] << *bot_lb_reselct_after;
  (*nfm)["cmbx_reselect_after"] << *cmbx_reselect_after;

  // checkboxes
  (*nfm)["chbx_tele_home"] << *chbx_tele_home;
  if (f->tele_back_home) {
    (*nfm)["txt_tele_home"] << *txt_tele_home;
  }

  (*nfm)["chbx_tele_to_target"] << *chbx_tele_to_target;
  // somehow crashes client
  //(*nfm)["chbx_tele_target"] << *chbx_tele_target;

  (*nfm)["btn_enable"] << *btn_enable;

  nfm->collocate();
  fm->collocate();
}

void gui::miscs() {
  char buf[2048];
  delete nfm;

  nfm = new nana::nested_form(*fm, nana::appearance(0, 0, 0, 0, 0, 0, 0));

  miscs_lb_title = new nana::label(*nfm, "Miscs", true);
  miscs_lb_title->text_align(nana::align::center, nana::align_v::center);

  nana::checkbox *chbx_range = new nana::checkbox(
      *nfm, "Max range", true);
  chbx_range->check(f->is_range_activated());

  nana::checkbox *chbx_no_col = new nana::checkbox(
      *nfm, "No Collision", true);
  chbx_no_col->check(f->is_no_collision_activated());

  chbx_range->events().checked([this, chbx_range] {
    if (chbx_range->checked()) {
      printf("call `f->activate_range()`\n");
      f->activate_range();
    } else {
      printf("call `f->terminate_range()`\n");
      f->terminate_range();
    }

    // remove label controls
    destroy_miscs();

    // reload all
    miscs();
  });

  chbx_no_col->events().checked([this, chbx_no_col] {
    if (chbx_no_col->checked()) {
      printf("call `f->activate_no_collision()`\n");
      f->activate_no_collision();
    } else {
      printf("call `f->terminate_no_collision()`\n");
      f->terminate_no_collision();
    }

    // remove label controls
    destroy_miscs();

    // reload all
    miscs();
  });

  (*fm)["nfm"] << *nfm;

  sprintf(buf, "vert <><weight=24 lb_title>"
               // checkboxes
               "<weight=24 margin=[0,0,0,4] chbx_range>"
               "<weight=24 margin=[0,0,0,4] chbx_no_col>"
               "<><weight=24 "
               "margin=2 btn_enable>");

  nfm->div(buf);

  (*nfm)["lb_title"] << *miscs_lb_title;

  // checkboxes
  (*nfm)["chbx_range"] << *chbx_range;
  (*nfm)["chbx_no_col"] << *chbx_no_col;

  nfm->collocate();
  fm->collocate();
}